package com.example.demosecurity.auth;

import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Qualifier("websoket")
public class KeycloakWebSocketAuthManager implements AuthenticationManager {
    private static final Logger log = LoggerFactory.getLogger(KeycloakWebSocketAuthManager.class);

    private final KeycloakTokenVerifier tokenVerifier;

    public KeycloakWebSocketAuthManager(final KeycloakTokenVerifier tokenVerifier) {
        this.tokenVerifier = tokenVerifier;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        JWSAuthenticationToken token = (JWSAuthenticationToken) authentication;
        String tokenString = (String) token.getCredentials();
        try {
            AccessToken accessToken = tokenVerifier.verifyToken(tokenString);
            List<GrantedAuthority> authorities = accessToken.getRealmAccess()
                    .getRoles()
                    .stream()
                    .map(r -> "ROLE_" + r) // fixme: wtf?
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
            token = new JWSAuthenticationToken(tokenString, authorities);
            token.setAuthenticated(true);
        } catch (VerificationException e) {
            log.debug("Exception authenticating the token {}:", tokenString, e);
            throw new BadCredentialsException("Invalid token");
        }
        return token;
    }
}
