package com.example.demosecurity.auth;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class JWSAuthenticationToken extends AbstractAuthenticationToken {
    private static final long serialVersionUID = 1L;

    private String token;

    public JWSAuthenticationToken(String token) {
        this(token, null);
    }

    public JWSAuthenticationToken(String token, Collection<GrantedAuthority> authorities) {
        super(authorities);
        this.token = token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    public String getToken() {
        return token;
    }
}
