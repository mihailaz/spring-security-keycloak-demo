package com.example.demosecurity;

import com.example.demosecurity.auth.JWSAuthenticationToken;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;

import java.util.Optional;

@Configuration
public class SecurityWebSocketConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {

    @Qualifier("websocket")
    private final AuthenticationManager authenticationManager;

    public SecurityWebSocketConfig(final AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
        messages.nullDestMatcher().permitAll()
                .simpDestMatchers("/app/public").permitAll()
                .simpDestMatchers("/app/secured").authenticated()
                .simpDestMatchers("/app/admin").hasRole("admin")
                .simpSubscribeDestMatchers("/topic/public").permitAll()
                .simpSubscribeDestMatchers("/topic/secured").authenticated()
                .simpSubscribeDestMatchers("/topic/admin").hasRole("admin")
                .anyMessage().permitAll();
    }

    @Override
    protected boolean sameOriginDisabled() {
        return true;
    }

    @Override
    public void customizeClientInboundChannel(ChannelRegistration registration) {
        registration.interceptors(new ChannelInterceptor() {
            @Override
            public Message<?> preSend(Message<?> message, MessageChannel channel) {
                StompHeaderAccessor accessor =
                        SimpMessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
                if (accessor != null && StompCommand.CONNECT.equals(accessor.getCommand())) {
                    Optional.ofNullable(accessor.getNativeHeader("Authorization"))
                            .ifPresent(ah -> {
                                String bearerToken = ah.get(0).replace("Bearer ", "");
                                JWSAuthenticationToken token =
                                        (JWSAuthenticationToken) authenticationManager.authenticate(
                                                new JWSAuthenticationToken(bearerToken));
                                accessor.setUser(token);
                            });
                }
                if (accessor != null && StompCommand.SEND.equals(accessor.getCommand())) {
                    JWSAuthenticationToken token = (JWSAuthenticationToken) authenticationManager.authenticate(
                            (JWSAuthenticationToken) accessor.getUser());
                    accessor.setUser(token);
                }
                return message;
            }
        });
    }
}
