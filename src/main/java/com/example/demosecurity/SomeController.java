package com.example.demosecurity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class SomeController {
    private static final Logger log = LoggerFactory.getLogger(SomeController.class);

    @GetMapping("/public")
    public Response _public(final Principal principal) {
        log.info("public controller received request with " + principal);
        return new Response(principal == null ? "invalid token" : "successfully logged");
    }

    @GetMapping("/secured")
    public Response secured(final Principal principal) {
        log.info("secured controller received request with " + principal);
        return new Response(principal == null ? "invalid token" : "successfully logged");
    }

    @GetMapping("/admin")
    public Response admin(final Principal principal) {
        log.info("admin controller received request with " + principal);
        return new Response(principal == null ? "invalid token" : "successfully logged");
    }

    public static class Response {
        private final String msg;

        public Response(final String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }
    }
}
