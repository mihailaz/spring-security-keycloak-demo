package com.example.demosecurity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import java.security.Principal;

@Controller
public class WsController {
    private static final Logger log = LoggerFactory.getLogger(WsController.class);

    @MessageMapping("/public")
    public void _public(final WsMessage msg, final Principal principal) {
        log.info("public web socket received {}, with principal {}", msg, principal);
    }

    @MessageMapping("/secured")
    public void secured(final WsMessage msg, final Principal principal) {
        log.info("secured web socket received {}, with principal {}", msg, principal);
    }

    @MessageMapping("/admin")
    public void admin(final WsMessage msg, final Principal principal) {
        log.info("admin web socket received {}, with principal {}", msg, principal);
    }

    public static class WsMessage {
        private String name;

        public WsMessage() {}

        public String getName() {
            return name;
        }

        public void setName(final String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "WsMessage{" + "name='" + name + '\'' +
                    '}';
        }
    }
}
