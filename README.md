### Instruction
- install docker & docker-compose
- docker-compose up -d
- go to http://localhost:8081/auth and create keycloak client, user, etc.
- start spring app
- run requests from _http/auth.http
